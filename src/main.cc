#include <getopt.h>

#include "seq2feature.h"

static option opts[] = {
  {"index",   required_argument, NULL, 'i'},
  {"output",  required_argument, NULL, 'o'},
  {"type",    required_argument, NULL, 't'},
  {"window",  required_argument, NULL, 'w'},
  {"flag",    no_argument,       NULL, 'f'},
  {"threads", no_argument,       NULL, 'n'},
  {"help",    no_argument,       NULL, 'h'},
  {0, 0, 0, 0},
  {0, 0, 0, 0},
  {0, 0, 0, 0}
};

int main(int argc, char* argv[]) {
  long start = clock();
  if (argc < 2) { 
    printf("Usage:\n");
    printf("leri_extract -i <list_file> -o <output_directory> -f <download_or_not> -w <slide_window_size> -n <number_of_threads>\n");
    exit(-1);
  }
  // char* index  = NULL;
  string index  = "";
  string output = "";
  string type   = "";

  bool flag = false;
  int num = 1;
  int w   = 1;
  char var = 0;
  int opt_idx = 0;
  while ((var = getopt_long(argc, argv, ":h:i:o:t:w:f:n:", opts, &opt_idx)) != EOF) {
    if (var == 'i') { string tmp(optarg); index  = tmp; }
    if (var == 'o') { string tmp(optarg); output = tmp; }
    if (var == 't') { string tmp(optarg); type   = tmp; }
    if (var == 'w') { w      = atoi(optarg); }
    if (var == 'f') { flag   = atoi(optarg); }
    if (var == 'n') { num    = atoi(optarg); }
  }

  printf("%s\n", index.c_str());
  printf("%s\n", output.c_str());
  char str_end = output[output.length()-1];
  if (str_end != '/') {
    output += "/";
  }
  if (!boost::filesystem::exists(output)) {
    boost::filesystem::create_directories(output);
  }
  collect_pdb(output, index, flag);
  seq2dat(output, num);
  build_data<double>(output, w, type);
  return 0;
}
