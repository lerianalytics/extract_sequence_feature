/**
 * The C++ Package of LERI (Learning Engine to Recognize Life).
 * Copyright (C) 2015- All Rights Reserved - Yaan J. Jang.
 * Contact: yaan.jang 'AT' gmail.com
 *
*/

#ifndef LERI_SEQ2FEATURE_H_
#define LERI_SEQ2FEATURE_H_

//#include "leri/common.h"
//#include "leri/util/fileutil.h"
//#include "leri/util/matrix.h"
//#include "leri/gio/figure.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <math.h> 
#include <regex>

#include "util.h"
using namespace std;
//namespace leri {


void collect_pdb(string out_dirt, string pdb_cull_name, bool dwl_flag) {
  if (dwl_flag) {
    printf("-- Leri is downloading PDB file from RCSB Protein Data Bank ... ");
  }
  // fflush(stdout);
  ifstream pdb_cull_fid;
  string line_pdb_cull;

  string wgetstr;
  const char* cmd;
  string pdb_idx_name = out_dirt + "leri_pdb.index";
  ofstream pdb_idx_fid;
  pdb_idx_fid.open(pdb_idx_name.c_str(), ios::trunc);
  if (!pdb_idx_fid) {
    printf( "\n\033[1;31m");
    printf( "Error: ");
    printf( "Leri cannot find the pdb index file!\n");
    printf( "\033[0m\n");
    exit (EXIT_FAILURE);
  }
  if (pdb_idx_fid.is_open()) {
    pdb_idx_fid << "";
    pdb_idx_fid.close();
  }
  pdb_cull_fid.open(pdb_cull_name.c_str());
  if (!pdb_cull_fid) {
    printf( "\n\033[1;31m");
    printf( "Error: ");
    printf( "Leri cannot find culled pdb index file!\n");
    printf( "\033[0m\n");
    exit (EXIT_FAILURE);
  }
  printf("%s\n", out_dirt.c_str());
  if (pdb_cull_fid.is_open()) {
    if (!boost::filesystem::exists(out_dirt+"pdb")) {
      boost::filesystem::create_directory(out_dirt+"pdb");
    }
    int num = 0;
    getline(pdb_cull_fid, line_pdb_cull); // Skip 1st line
    while (getline(pdb_cull_fid, line_pdb_cull)) {
      num++;
    }
    pdb_cull_fid.clear();
    pdb_cull_fid.seekg(0, ios::beg);
    getline(pdb_cull_fid, line_pdb_cull); // Skip 1st line
    int cnt = 0;
    while (getline(pdb_cull_fid, line_pdb_cull)) {
      string pdb_id = line_pdb_cull.substr(0,4);
      string chain  = line_pdb_cull.substr(4,1);
      if (dwl_flag) {
        wgetstr = "wget -nc http://www.rcsb.org/pdb/files/" 
                + pdb_id + ".pdb -O " 
                + out_dirt + "pdb/" + pdb_id + ".pdb -q";
        cmd = wgetstr.c_str();
        if (system(cmd)){}
      }
      pdb_idx_fid.open(pdb_idx_name.c_str(), ios::app);
      if (pdb_idx_fid.is_open()) {
        pdb_idx_fid << pdb_id << chain << "\n";
        pdb_idx_fid.close();
      }
      progress_bar<double>("Leri is collecting PDB (" + pdb_id + ")", cnt+1, num, "files");
      cnt++;
    }
    progress_bar<double>("Leri is collecting PDBs (All)", num, num, "files");
    pdb_cull_fid.close();
  }
  printf(".\n");
  // printf("-- Leri saved the pdb index in the file: \n-- <%s>\n", pdb_idx_name.c_str());
}

void seq2dat(string out_dirt, int num_threads) {
  // printf("-- Leri is extracting protein torsion angles from the PDB file ... ");
  // fflush(stdout);
  string lib_dirt = string(LIB_DIRT) + "/";

  string dirt_pdb   = out_dirt + "pdb/";
  string dirt_fasta = out_dirt + "fasta/";
  string dirt_pssm  = out_dirt + "pssm/";
  string dirt_ss    = out_dirt + "ss/";
  string dirt_sasa  = out_dirt + "sasa/";
  string dirt_phsi  = out_dirt + "phsi/";
  string dirt_mat   = out_dirt + "mat/";

  if (!boost::filesystem::exists(dirt_pdb)) {
    boost::filesystem::create_directory(dirt_pdb);
  }
  if (!boost::filesystem::exists(dirt_fasta)) {
    boost::filesystem::create_directory(dirt_fasta);
  }
  if (!boost::filesystem::exists(dirt_pssm)) {
    boost::filesystem::create_directory(dirt_pssm);
  }
  if (!boost::filesystem::exists(dirt_ss)) {
    boost::filesystem::create_directory(dirt_ss);
  }
  if (!boost::filesystem::exists(dirt_sasa)) {
    boost::filesystem::create_directory(dirt_sasa);
  }
  if (!boost::filesystem::exists(dirt_phsi)) {
    boost::filesystem::create_directory(dirt_phsi);
  }
  if (!boost::filesystem::exists(dirt_mat)) {
    boost::filesystem::create_directory(dirt_mat);
  }

  string bin_stride = lib_dirt  + "bin/stride -r";
 
  string dbname       = lib_dirt + "nr/nr.filter";  // Nov. 16, 2015, which data set?
  string dirt_blast   = lib_dirt + "ncbi-blast-2.7.1+/bin/";
  string dirt_psipred = lib_dirt + "psipred-4.0.2/";

  string pidx, chain;
  string line_pdb_id;

  string seq_stride, phsi_stride;
  string cmd_str;
  const char* cmd;
  string pdb_idx_name = out_dirt + "leri_pdb.index";
  ifstream fid_pdb;
  fid_pdb.open(pdb_idx_name);
  if (fid_pdb.is_open()) {
    int num = 0;
    while (getline(fid_pdb, line_pdb_id)) {
      num++;
    }
    fid_pdb.clear();
    fid_pdb.seekg(0, ios::beg);
    int cnt = 0;
    while (getline(fid_pdb, line_pdb_id) && line_pdb_id.length() > 0) {
      pidx = line_pdb_id.substr(0,4);
      chain = line_pdb_id.substr(4,1);
      // FASTA
      cmd_str = bin_stride + chain + " -q " 
              + dirt_pdb + pidx + ".pdb > " 
              + dirt_fasta + pidx + chain + ".fasta";
      cmd = cmd_str.c_str();
      if (system(cmd)){}

      // Phi and Psi
      // progress_bar<double>("Leri is extracting torsion anlges", cnt+1, num, "PDBs");
      cmd_str = bin_stride + chain + " " 
              + dirt_pdb + pidx + ".pdb | grep \"ASG \" | awk '{if($3 ==\"" 
              + chain + "\") printf(\"%5i %3s %3.2f %3.2f\\n\" ,$5, $2, $8, $9)}' > " 
              + dirt_phsi + pidx + chain  + ".phsi";
      cmd = cmd_str.c_str();
      if (system(cmd)) {}
      progress_bar<double>("Leri is extracting " + pidx + ": phi/psi", cnt, num, "PDBs");

      // Position specific scoring matrix
      cmd_str = dirt_blast + "psiblast "
              + "-db " + dbname + " "
              + "-query " + dirt_fasta + pidx + chain + ".fasta "
              + "-num_iterations 3 "
              + "-num_threads " + to_string(num_threads) + " "
              + "-inclusion_ethresh 0.001 "
              + "-num_descriptions 5000 "
              + "-num_alignments 0 "
              + "-save_pssm_after_last_round "
//          + "-out_pssm " + out_dirt + "psitmp.chk "
              + "-out_ascii_pssm " + dirt_pssm + pidx + chain + ".mat3 > /dev/null";
              //+ "-out_ascii_pssm " + dirt_pssm + pidx + chain + ".mat3 > /dev/null 2>&1 &";
      cmd = cmd_str.c_str();
      if (system(cmd)) {}
      progress_bar<double>("Leri is extracting " + pidx + ": PSSM   ", cnt, num, "PDBs");

      // Secondary structure
      cmd_str = dirt_psipred + "bin/seq2mtx "
              + dirt_fasta + pidx + chain + ".fasta > "
              + dirt_mat + pidx + chain + ".mat";
      cmd = cmd_str.c_str();
      if (system(cmd)){}
      cmd_str = dirt_psipred + "bin/psipred "
              + dirt_mat + pidx + chain + ".mat "
              + dirt_psipred + "data/weights.dat "
              + dirt_psipred + "data/weights.dat2 "
              + dirt_psipred + "data/weights.dat3 > "
              + dirt_ss + pidx + chain + ".ss";
      cmd = cmd_str.c_str();
      if (system(cmd)){}
      cmd_str = dirt_psipred + "bin/psipass2 "
              + dirt_psipred + "data/weights_p2.dat 1 1.0 1.0 "
              + dirt_ss + pidx + chain + ".ss2 "
              + dirt_ss + pidx + chain + ".ss > "
              + dirt_ss + pidx + chain + ".horiz";
      cmd = cmd_str.c_str();
      if (system(cmd)) {}
      progress_bar<double>("Leri is extracting " + pidx + ": SS     ", cnt, num, "PDBs");

      // Solvent accessiable surface area
      cmd_str = dirt_psipred + "bin/solvpred "
              + dirt_mat + pidx + chain + ".mat "
              + dirt_psipred + "data/weights_solv.dat > "
              + dirt_sasa + pidx + chain + ".sa";
      cmd = cmd_str.c_str();
      if (system(cmd)) {}
      progress_bar<double>("Leri is extracting " + pidx + ": SA     ", cnt, num, "PDBs");
      cnt++;
    }
    fid_pdb.close();
    progress_bar<double>("Leri is extracting sequence features", cnt, num, "PDBs");
  }
  printf(".\n");
}

template<typename Dtype>
void read_data(vector<vector<Dtype> >& mtx, string file_name, int read_flag) {
  int nrow = 0;
  vector<string> str;
  string line;
  ifstream fid(file_name.c_str());
  if (fid.is_open()) {
    while (getline(fid, line)) {
      if(line.length()) { 
        str.push_back(line);
      }
    }
    fid.close();
  }
  nrow = str.size();
  FILE * fid_out;
  fid_out = fopen(file_name.c_str(), "w+");
  for (int i = 0; i < nrow; i++) {
    switch (read_flag) {
      case 0: { // PSSM
        if (i < 2) {
          fprintf(fid_out, "# %s\n", str[i].c_str());
        } else if (i >= str.size()-5) {
          fprintf(fid_out, "# %s\n", str[i].c_str());
        } else {
          fprintf(fid_out, "# %s\n", str[i].substr(0, 8).c_str());
          line = str[i].substr(9, 80);
          line = regex_replace(line, regex("   "), " ");
          line = regex_replace(line, regex("  "), " ");
          line = boost::trim_copy(line);
          fprintf(fid_out, "%s\n", line.c_str());
        }
        break;
      }
      case 1: { // SS
        if (i < 1) {
          fprintf(fid_out, "# %s\n", str[i].c_str());
        } else {
          fprintf(fid_out, "# %s\n", str[i].substr(0, 10).c_str());
          line = str[i].substr(11);
          line = boost::trim_copy(line);
          line = regex_replace(line, regex("   "), " ");
          line = regex_replace(line, regex("  "), " ");
          fprintf(fid_out, "%s\n", line.c_str());
        }
        break;
      }
      case 2: { // SA
        fprintf(fid_out, "# %s\n", str[i].substr(0, 7).c_str());
        fprintf(fid_out, "%s\n", str[i].substr(8).c_str());
        break;
      }
      case 3: { // PHI & PSI
        fprintf(fid_out, "# %s\n", str[i].substr(0, 9).c_str());
        fprintf(fid_out, "%s\n", str[i].substr(10).c_str());
        break;
      }
    }
  }
  fclose(fid_out);
  nrow = 0;
  vector<vector<Dtype> > mat;
  switch (read_flag) {
    case 0: { // PSSM
      nrow = str.size()-7;
      read_matrix_from_file(mat, file_name, ' ');
      #ifdef USE_OPENMP
      #pragma omp parallel for 
      #endif
      for (int i = 0; i < nrow; i++) {
        for (int j = 0; j < 20; j++) {
          mtx[i][j] = mat[i][j];
        }
      }
      break;
    }
    case 1: { // SS
      nrow = str.size()-1;
      read_matrix_from_file(mat, file_name, ' ');
      #ifdef USE_OPENMP
      #pragma omp parallel for 
      #endif
      for (int i = 0; i < nrow; i++) {
        for (int j = 0; j < 3; j++) {
          mtx[i][j+20] = mat[i][j];
        }
      }
      break;
    }
    case 2: { // SA
      nrow = str.size();
      read_matrix_from_file(mat, file_name, ' ');
      #ifdef USE_OPENMP
      #pragma omp parallel for 
      #endif
      for (int i = 0; i < nrow; i++) {
        for (int j = 0; j < 1; j++) {
          mtx[i][j+23] = mat[i][j];
        }
      }
      break;
    }
    case 3: { // PHI & PSI
      nrow = str.size();
      read_matrix_from_file(mat, file_name, ' ');
      #ifdef USE_OPENMP
      #pragma omp parallel for 
      #endif
      for (int i = 0; i < nrow; i++) {
        // for (int j = 0; j < 4; j += 2) {
          mtx[i][24+0] = sin(mat[i][0]*DEGREES_TO_RADIANS);
          mtx[i][24+1] = cos(mat[i][0]*DEGREES_TO_RADIANS);
          mtx[i][24+2] = sin(mat[i][1]*DEGREES_TO_RADIANS);
          mtx[i][24+3] = cos(mat[i][1]*DEGREES_TO_RADIANS);
        // }
      }
      break;
    }
  }
}

template <typename Dtype> 
void build_data(string out_dirt, int win_size, string dat_typ) {
  printf("-- Leri is building data ... ");
  fflush(stdout);
  int read_flag; // PSSM: 0; SS: 1; SA:2
  // string dirt_pdb   = out_dirt + "pdb/";
  // string dirt_fasta = out_dirt + "fasta/";
  string dirt_pssm  = out_dirt + "pssm/";
  string dirt_ss    = out_dirt + "ss/";
  string dirt_sasa  = out_dirt + "sasa/";
  string dirt_phsi  = out_dirt + "phsi/";
  string dirt_data  = out_dirt + "data/";
  if (!boost::filesystem::exists(dirt_data)) {
    boost::filesystem::create_directory(dirt_data);
  }

  ifstream pdb_idx_fid;
  string line_pdb_idx;
  string pdb_id, chain;//, fnam;
  string fid_name;

  vector<vector<Dtype> > dat_block;
  vector<vector<Dtype> > mtx;

  string matrix_name;
  string postfix;

  if (true) {
    string pdb_idx_name = out_dirt + "leri_pdb.index";
    pdb_idx_fid.open(pdb_idx_name.c_str());
    if (pdb_idx_fid.is_open()) {
      int num = 0;
      while (getline(pdb_idx_fid, line_pdb_idx)) {
        num++;
      }
      pdb_idx_fid.clear();
      pdb_idx_fid.seekg(0, ios::beg);
      int cnt = 0;
      while (getline(pdb_idx_fid, line_pdb_idx)) { 
        if(!dat_typ.compare("test")) {
          pdb_id = line_pdb_idx;
          chain = "";
        }
        else {
          pdb_id = line_pdb_idx.substr(0, 4);
          chain  = line_pdb_idx.substr(4, 1);
        }
        // Read PSSM
        read_flag = 0;
        fid_name = dirt_pssm + pdb_id + chain + ".mat3";// 1N7S-3TJ4A.pssm.tmp
        string line;
        int n_aa = 0;
        ifstream p_fid;
        p_fid.open(fid_name.c_str());
        if (p_fid.is_open()) {
          while (getline(p_fid, line)) {
            n_aa++;
          }
          p_fid.close();
        }

        mtx.resize(n_aa-9, vector<Dtype>(28, 0.0)); //PSSM (20) + SS (3) + SA (1) + sin(PHI) (1) + cos(PHI) (1) + sin(PSI) (1) + cos(PSI) (1)
        n_aa = 0;
        read_data(mtx, fid_name, read_flag);
        // Read SS
        read_flag = 1;
        fid_name = dirt_ss + pdb_id + chain + ".ss2";// 1N7S-3TJ4A.pssm.tmp
        read_data(mtx, fid_name, read_flag);
        // Read SA
        read_flag = 2;
        fid_name = dirt_sasa + pdb_id + chain + ".sa";// 1N7S-3TJ4A.pssm.tmp
        read_data(mtx, fid_name, read_flag);
        // Read PHI and PSI
        if(!dat_typ.compare("train")) {
          postfix = "_pssm_ss_sa_phsi";
          read_flag = 3;
          fid_name = dirt_phsi + pdb_id + chain + ".phsi";// 1N7S-3TJ4A.pssm.tmp
          read_data(mtx, fid_name, read_flag);
        }

        if(!dat_typ.compare("test")) {
          postfix = "_pssm_ss_sa";
        }
        int num_out = 4;
        vector<Dtype> vect(24*(2*win_size+1)+num_out, 0.0);
        for (int i = 0; i < mtx.size(); i++) {
          if (i < win_size) { // N-terminal
            vect.assign(vect.size(), 0.0);
            for (int j = 0; j <= win_size-i; j++) { // Left and center
              for (int k = 0; k < 24; k++) {
                vect[num_out+j*24+k] = mtx[i][k];
              }
            }
            for (int j = 0; j < win_size; j++) {  // Right
              for (int k = 0; k < 24; k++) {
                vect[num_out+(win_size+1+j)*24+k] = mtx[i+j+1][k];
              }
            }
          } else if (mtx.size()-i <= win_size) { // C-terminal
            vect.assign(vect.size(), 0.0);
            for (int j = 0; j <= win_size; j++) {  // Left and center
              for (int k = 0; k < 24; k++) {
                vect[num_out+j*24+k] = mtx[i-win_size+j][k];
              }
            }
            for (int j = 0; j < win_size; j++) { // Right
              for (int k = 0; k < 24; k++) {
                vect[num_out+(win_size+1+j)*24+k] = mtx[i][k];
              }
            } 
          } else {
            vect.assign(vect.size(), 0.0);
            for (int j = -win_size; j <= win_size; j++) {
              for (int k = 0; k < 24; k++) {
                vect[num_out+(win_size+j)*24+k] = mtx[i+j][k];
              }
            }
          }
          if(!dat_typ.compare("train")) {
            vect[0] = mtx[i][mtx[i].size()-4];
            vect[1] = mtx[i][mtx[i].size()-3];
            vect[2] = mtx[i][mtx[i].size()-2];
            vect[3] = mtx[i][mtx[i].size()-1];
          }
          if(!dat_typ.compare("test")) {
            vect[0] = 0.0;
            vect[1] = 0.0;
            vect[2] = 0.0;
            vect[3] = 0.0;
          }
          dat_block.push_back(vect);
        }
        mtx.clear();
        vect.clear();
        matrix_name = dirt_data + pdb_id + chain + postfix;
        write_matrix<Dtype>(matrix_name, dat_block, (int)dat_block.size(), (int)dat_block[0].size(), "sin(PHI) cos(PHI) sin(PSI) cos(PSI) PSSM SS SA");
        dat_block.clear();
        cnt++;
        progress_bar<double>("Leri is building data (" + pdb_id + ")", cnt, num);
      }
      pdb_idx_fid.close();
    }
  } 
  printf(".\n");
}



//} // namespace leri
#endif // LERI_SEQ2FEATURE_H_
