#include <iostream>
#include<string>

// Boost Library
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

// #define VERSION AS_STRING(VERSION)
// #define VERSION "2.4.9"
// PI / 180
#define DEGREES_TO_RADIANS 0.017453292519943295769236907684886
using namespace std;

template <typename Dtype>
void progress_bar(string str, const int idx = -1, const int max_num = -1, string postfix = "", Dtype fval = -999.0, int format = 0) {
// void progress_bar (Dtype percentage, string str, int val = -1, string postfix = "", Dtype fval = -999.0) {
  const static string progress_bar_ = "===========================";
  const static int progress_bar_width_  = 27;
  Dtype percentage = (1.0*idx/max_num);
  int pct = (int) (percentage * 100);
  int lpad = (int) (percentage * progress_bar_width_);
  int rpad = progress_bar_width_ - lpad;
  if (idx == -1) {
    printf ("\r-- %s[%.*s%*s] %3d%% completed", str.c_str(), lpad, progress_bar_.c_str(), rpad, "", pct);
  } else if (fval == -999.0) {
    if (!postfix.empty()) {
      printf ("\r-- %s (%d/%d %s) [%.*s%*s] %3d%% completed", str.c_str(), idx, max_num, postfix.c_str(), lpad, progress_bar_.c_str(), rpad, "", pct);
    } else {
      printf ("\r-- %s (%d/%d) [%.*s%*s] %3d%% completed", str.c_str(), idx, max_num, lpad, progress_bar_.c_str(), rpad, "", pct);
    }
  } else {
    if (format == 0) {
      printf ("\r-- %s %d/%d %s %.3f [%.*s%*s] %3d%% completed", str.c_str(), idx, max_num, postfix.c_str(), fval, lpad, progress_bar_.c_str(), rpad, "", pct);
    } else if (format == 1) {
      printf ("\r-- %s %d/%d %s %.3f%% [%.*s%*s] %3d%% completed", str.c_str(), idx, max_num, postfix.c_str(), fval*100, lpad, progress_bar_.c_str(), rpad, "", pct);
    }
    else {
      printf ("\r-- %s %d/%d %s %.2e [%.*s%*s] %3d%% completed", str.c_str(), idx, max_num, postfix.c_str(), fval, lpad, progress_bar_.c_str(), rpad, "", pct);
    }
  }
  fflush (stdout);
}

static inline void trim_left(char *str, char c) {
  char *end;
  int len,i;

  len = strlen(str);
  while (*str && len) {
    end = str + len - 1;
    if(c == *str) {
      for(i=0;i<len-1;i++) {
        str[i]=str[i+1]; 
      }
      *end = '\0';
    }
    else
      break;
    len = strlen(str);
  }
}

static inline void trim_right(char* str, char c) {
  char *end;
  int len;
  len = strlen(str);
  while (*str && len) {
    end = str + len - 1;
    if(c == *end)
      *end = '\0';
    else
      break;
    len = strlen(str);
  }
}

static inline void trim_all(char *str) {
  //NOTE: trim_left must be passed by reference and trim_right must be passed by value
  trim_left(str, 0x09);   // horizontal tab    
  trim_left(str, 0x0b);   // vertical tab
  trim_left(str, 0x0c);   // form feed
  trim_left(str, 0x20);   // space

  trim_right(str, 0x09);   // horizontal tab
  trim_right(str, 0x0a);   // linefeed
  trim_right(str, 0x0b);   // vertical tab
  trim_right(str, 0x0c);   // form feed
  trim_right(str, 0x0d);   // carriage return
  trim_right(str, 0x20);   // space
}

void trim_all(string* str) {
  char* str_trim = NULL;
  str_trim = &(*str->begin());
  trim_all(str_trim);
  *str = string(str_trim);
}

template <typename Dtype> 
void write_matrix(string out, vector<vector<Dtype> >& mat, int nrow, int ncol, string str = "", string delimiter = ",", string postfix = ".csv") {
  out += postfix;
  FILE* fid = fopen(out.c_str(), "w");
  if (str.compare("")) {
    fprintf(fid, "# %s (Leri Analytics by yaan.jang@gmail.com)\n\n", str.c_str());
  }
  for (int i = 0; i < nrow; i++) {
    for (int j = 0; j < ncol; j++) {
      fprintf(fid, "%g", mat[i][j]);
      if (j < ncol-1) { fprintf(fid, "%s", delimiter.c_str()); }
      else { fprintf(fid, "\n"); }
    }
  }
  fclose(fid);
}

template <typename Dtype> 
void read_matrix_from_file(vector<vector<Dtype> >& mat, string file, char delimiter) {
  mat.clear();
  ifstream fid_in;
  string line;
  int row = 0;
  vector<Dtype> vect;
  fid_in.open(file, ifstream::in);
  if (fid_in.is_open()) {
    while (fid_in) {
      if (!getline(fid_in, line)) break;
      trim_all(&line);
      if (line[0] == '#' || line.length() < 1) continue;
      stringstream ss(line);
      vector <string> str;
      string subline;
      while (ss) {
        if (!getline(ss, subline, delimiter)) break;
        str.push_back(subline);
      }
      for (int i = 0; i < str.size(); i++) {
        vect.push_back(atof(str[i].c_str()));
      }
      mat.push_back(vect);
      vect.clear();
      ++row;
    }
    fid_in.close();
  } else {
    printf("--  \033[1;31m");
    printf("Error in loading data from: \"%s\"", file.c_str());
    printf("\033[0m\n");
    exit (EXIT_FAILURE);
  }
}
